// Listeners
document.getElementById("Clean").addEventListener('click', function() {cleanTabs()}, false);

// Variables
var urls = [];
var bases = [];

var last = 0;

var ids = [];


// Functions
function cleanTabs() {
    urls = [];
    bases = [];
    ids = [];
    
    chrome.tabs.getAllInWindow(null, function(tabs) {
        for (var i = 0; i < tabs.length; i++) {
            parseTab(tabs[i]);
        }
        for (var i = 0; i < bases.length; i++) {
            for (var y = 0; y < ids[i].length; y++) {
                chrome.tabs.move(ids[i][y], {"index": y});
            }
        }
    });
}
function parseTab(tab) {
    var base = getBaseUrl(tab.url);
    var url = tab.url;
    
    if (urls.indexOf(url) == -1) {
        urls[urls.length] = url;
        
        var index = bases.indexOf(base);
        if (index == -1) {
            bases[bases.length] = base;
            ids[bases.length - 1] = [];
            ids[bases.length - 1][ids[bases.length - 1].length] = tab.id;
        } else {
            ids[index][ids[index].length] = tab.id;
        }
    } else {
        chrome.tabs.remove(tab.id);
    }
}
function getBaseUrl(url) {
    var base = url.split('.');
    
    if (base[0] == "http://www"
    || base[0] == "https://www"
    || base[0] == "www") {
        return base[1];
    } else {
        return base[0];
    }
}